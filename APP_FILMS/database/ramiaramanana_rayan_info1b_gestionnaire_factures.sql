-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 18 Avril 2021 à 12:06
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ramiaramanana_rayan_info1b_gestionnaire_factures`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_creancier`
--
DROP DATABASE IF EXISTS ramiaramanana_rayan_info1b_gestionnaire_factures;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ramiaramanana_rayan_info1b_gestionnaire_factures;

-- Utilisation de cette base de donnée

USE ramiaramanana_rayan_info1b_gestionnaire_factures;

CREATE TABLE `t_creancier` (
  `id_creancier` int(11) NOT NULL,
  `raison_sociale` varchar(25) NOT NULL,
  `reference_bvr` varchar(46) NOT NULL,
  `iban` varchar(21) NOT NULL,
  `numero_telephone` int(10) NOT NULL,
  `adresse` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_creancier`
--

INSERT INTO `t_creancier` (`id_creancier`, `raison_sociale`, `reference_bvr`, `iban`, `numero_telephone`, `adresse`) VALUES
(1, 'Micro Informatic SA', '33 12340 00000 00000 00001 23153', 'CH2180808001234567890', 244474470, 'En Chamard 41a 1442 Montagny, Yverdon-les-Bains');

-- --------------------------------------------------------

--
-- Structure de la table `t_facture`
--

CREATE TABLE `t_facture` (
  `id_facture` int(11) NOT NULL,
  `fk_creancier` int(11) NOT NULL,
  `numero_facture` int(25) NOT NULL,
  `reference_bvr` varchar(60) NOT NULL,
  `montant_facture` float NOT NULL,
  `motif_facturation` text NOT NULL,
  `echeance_facture` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_facture_en_attentte`
--

CREATE TABLE `t_facture_en_attentte` (
  `id_facture_en_attente` int(11) NOT NULL,
  `fk_utilisateur` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `date_enregistrement_facture` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_payer_facture`
--

CREATE TABLE `t_payer_facture` (
  `id_payer_facture` int(11) NOT NULL,
  `fk_utilisateur` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `date_payement_facture` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_utilisateur`
--

CREATE TABLE `t_utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom_utilisateur` varchar(25) NOT NULL,
  `mot_de_passe` varchar(25) NOT NULL,
  `banque_utilisateur` varchar(15) NOT NULL,
  `numero_bancaire` float NOT NULL,
  `cryptogramme_visuel` int(4) NOT NULL,
  `date_expiration_carte` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_utilisateur`
--

INSERT INTO `t_utilisateur` (`id_utilisateur`, `nom_utilisateur`, `mot_de_passe`, `banque_utilisateur`, `numero_bancaire`, `cryptogramme_visuel`, `date_expiration_carte`) VALUES
(1, 'Admin', 'Admin', 'UBS', 1234570000, 0, '0000-00-00'),
(2, 'Rayan', '123456789', 'UBS', 987654000, 1234, '0000-00-00'),
(3, 'Red sus', '420', 'UBS', 4.69824e15, 1234, '2023-04-14');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_creancier`
--
ALTER TABLE `t_creancier`
  ADD PRIMARY KEY (`id_creancier`);

--
-- Index pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD PRIMARY KEY (`id_facture`),
  ADD KEY `creancier` (`fk_creancier`);

--
-- Index pour la table `t_facture_en_attentte`
--
ALTER TABLE `t_facture_en_attentte`
  ADD PRIMARY KEY (`id_facture_en_attente`),
  ADD KEY `fk_utilisateur` (`fk_utilisateur`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  ADD PRIMARY KEY (`id_payer_facture`),
  ADD KEY `fk_utilisateur` (`fk_utilisateur`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_utilisateur`
--
ALTER TABLE `t_utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_creancier`
--
ALTER TABLE `t_creancier`
  MODIFY `id_creancier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_facture`
--
ALTER TABLE `t_facture`
  MODIFY `id_facture` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_facture_en_attentte`
--
ALTER TABLE `t_facture_en_attentte`
  MODIFY `id_facture_en_attente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  MODIFY `id_payer_facture` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_utilisateur`
--
ALTER TABLE `t_utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD CONSTRAINT `t_facture_ibfk_1` FOREIGN KEY (`fk_creancier`) REFERENCES `t_creancier` (`id_creancier`);

--
-- Contraintes pour la table `t_facture_en_attentte`
--
ALTER TABLE `t_facture_en_attentte`
  ADD CONSTRAINT `t_facture_en_attentte_ibfk_1` FOREIGN KEY (`fk_utilisateur`) REFERENCES `t_utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `t_facture_en_attentte_ibfk_2` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

--
-- Contraintes pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  ADD CONSTRAINT `t_payer_facture_ibfk_1` FOREIGN KEY (`fk_utilisateur`) REFERENCES `t_utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `t_payer_facture_ibfk_2` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
