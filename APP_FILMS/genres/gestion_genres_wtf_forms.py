"""
    Fichier : gestion_facture_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "facture_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    raison_sociale_wtf = StringField("Renseigner la raison sociale ")
    reference_bvr_wtf = StringField("Renseigner le bvr")
    iban_wtf = StringField("Renseigner l'IBAN")
    numero_telephone_wtf = StringField("Renseigner le numéro de téléphone")
    adresse_wtf = StringField("Renseigner l'adresse physique")

    submit = SubmitField("Enregistrer le nouveau créancier")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "facture_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    raison_sociale_update_wtf = StringField("Renseigner la raison sociale ")
    reference_bvr_update_wtf = StringField("Modifier le BVR")
    iban_update_wtf = StringField("Modifier l'IBAN")
    numero_telephone_update_wtf = StringField("Modifier le numéro de téléphone")
    adresse_update_wtf = StringField("Modifier l'adresse physique")

    submit = SubmitField("Enregistrer genre")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "facture_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_creancier".
    """
    creancier_delete_wtf = StringField("Effacer ce créancier")
    submit_btn_del = SubmitField("Effacer creancier")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
